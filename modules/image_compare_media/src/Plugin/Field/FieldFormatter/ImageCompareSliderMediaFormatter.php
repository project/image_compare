<?php

namespace Drupal\image_compare_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\image_compare\Plugin\Field\FieldFormatter\ImageCompareSliderFormatter;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the 'Image Compare Slider' formatter for media.
 *
 * @FieldFormatter(
 *   id = "image_compare_slider_media",
 *   label = @Translation("Image Compare Accessible Media Slider"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ImageCompareSliderMediaFormatter extends ImageCompareSliderFormatter {

  use ImageCompareSliderMediaFormatterTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $entities = parent::getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($entities)) {
      return [];
    }

    $settings = $this->getSettings();

    $style = $this->getImageStyle($settings);

    $cache_tags = is_null($style) ? [] : $style->getCacheTags();

    $options = $this->buildTemplateOptions($items, $cache_tags);

    $images = [];

    foreach ($entities as $media) {
      $source_field_name = $media->getSource()
        ->getConfiguration()['source_field'];
      $image_item = $media->get($source_field_name)[0];
      // Experimental handling of media entities referencing other entities.
      // See: https://www.drupal.org/project/media_entity_reference.
      if (($image_item instanceof EntityReferenceItem)
        && !($image_item instanceof ImageItem)) {
        $target_entity = $image_item->entity;
        // We only handle referenced entities of type Media.
        if ($target_entity instanceof MediaInterface) {
          $target_source_field_name = $target_entity->getSource()
            ->getConfiguration()['source_field'];
          $image_item = $target_entity->get($target_source_field_name)[0];
        }
      }
      // We only handle medias of type Image.
      if ($image_item instanceof ImageItem) {
        $image = $this->buildImageData($image_item, $media, $style);
        if (!is_null($image)) {
          $images[] = $image;
          $cache_tags = Cache::mergeTags(
            $cache_tags, $image_item->entity->getCacheTags());
        }

        if (count($images) == 2) {
          break;
        }
      }
    }

    return $this->buildRenderArray($images, $options, $cache_tags);
  }

}
