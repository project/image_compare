<?php

namespace Drupal\image_compare_responsive\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\image_compare\Plugin\Field\FieldFormatter\ImageCompareSliderFormatterTrait;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of the 'Image Compare Accessible Slider' responsive formatter.
 *
 * @FieldFormatter(
 *   id = "image_compare_slider_responsive",
 *   label = @Translation("Image Compare Accessible Slider Responsive"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageCompareSliderResponsiveFormatter extends ResponsiveImageFormatter {

  use ImageCompareSliderFormatterTrait;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The breakpoint manager.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected $breakpointManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->fileUrlGenerator = $container->get('file_url_generator');
    $instance->breakpointManager = $container->get('breakpoint.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function addImageStyleSelect(array &$elements, array $settings) {
    $responsive_image_options = [];
    $responsive_image_styles = $this->responsiveImageStyleStorage->loadMultiple();
    uasort($responsive_image_styles, '\Drupal\responsive_image\Entity\ResponsiveImageStyle::sort');
    if (!empty($responsive_image_styles)) {
      foreach ($responsive_image_styles as $machine_name => $responsive_image_style) {
        if ($responsive_image_style->hasImageStyleMappings()) {
          $responsive_image_options[$machine_name] = $responsive_image_style->label();
        }
      }
    }

    $elements['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#required' => TRUE,
      '#options' => $responsive_image_options,
      '#description' => [
        '#markup' => $this->linkGenerator->generate($this->t('Configure Responsive Image Styles'), new Url('entity.responsive_image_style.collection')),
        '#access' => $this->currentUser->hasPermission('administer responsive image styles'),
      ],
    ];
  }

  /**
   * Load ResponsiveImageStyle entity to use for display.
   *
   * @param array $settings
   *   Formatter settings.
   * @param array $cache_tags
   *   The cache tags to update with style tags.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The ImageStyle object.
   */
  protected function getResponsiveImageStyle(array $settings, array &$cache_tags) {
    try {
      $responsive_image_style = $this->responsiveImageStyleStorage->load($settings['image_style']);
      if ($responsive_image_style) {
        $cache_tags = Cache::mergeTags($cache_tags, $responsive_image_style->getCacheTags());
      }
      return $responsive_image_style;
    }
    catch (\Exception $e) {
      Error::logException($this->loggerFactory->get('image_compare'), $e);
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $files = parent::getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return [];
    }

    $settings = $this->getSettings();

    $cache_tags = [];

    /** @var \Drupal\responsive_image\Entity\ResponsiveImageStyle $style */
    $style = $this->getResponsiveImageStyle($settings, $cache_tags);
    $fallback_style = $this->loadImageStyle($style->getFallbackImageStyle());
    $breakpoints = array_reverse($this->breakpointManager->getBreakpointsByGroup($style->getBreakpointGroup()));

    $options = $this->buildTemplateOptions($items, $cache_tags);

    $images = [];
    foreach ($files as $file) {
      $item = $file->_referringItem;
      $image = $this->buildImageData($item, NULL, $fallback_style);
      if (!is_null($image)) {
        $variables['uri'] = $item->entity->getFileUri();
        $variables['width'] = $item->width;
        $variables['height'] = $item->height;
        foreach ($style->getKeyedImageStyleMappings() as $breakpoint_id => $multipliers) {
          if (isset($breakpoints[$breakpoint_id])) {
            $image['sources'][] = _responsive_image_build_source_attributes($variables, $breakpoints[$breakpoint_id], $multipliers);
          }
        }
        $images[] = $image;
        $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());
      }
      if (count($images) == 2) {
        break;
      }
    }

    return $this->buildRenderArray($images, $options, $cache_tags);
  }

}
