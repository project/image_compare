(($, Drupal, once) => {
  Drupal.behaviors.image_compare_slider = {
    attach(context) {
      once('image_compare', 'image-compare', context).forEach((element) => {
        const rangeInput = element.shadowRoot.querySelector('input');
        if (rangeInput) {
          rangeInput.id = `${element.id}_input`;
          rangeInput.parentElement.setAttribute('for', rangeInput.id);
          if (parseInt(element.getAttribute('start'), 10) > 0) {
            rangeInput.value = element.getAttribute('start');
          }
          rangeInput.dispatchEvent(new Event('change'));
          let mouseStep = 1;
          if (parseFloat(element.getAttribute('step')) < 1) {
            mouseStep = element.getAttribute('step');
            rangeInput.step = mouseStep;
          }
          let keyboardStep = 1;
          if (parseInt(element.getAttribute('keyboard_step'), 10) > 1) {
            keyboardStep = parseInt(element.getAttribute('keyboard_step'), 10);
          }
          if (mouseStep !== 1 || keyboardStep > 1) {
            rangeInput.addEventListener('keydown', (evt) => {
              // Only set the step on arrow key events.
              switch (evt.key) {
                case 'ArrowDown':
                case 'ArrowLeft':
                case 'ArrowRight':
                case 'ArrowUp':
                  rangeInput.step = keyboardStep;
                  break;
                default:
              }
            });
            rangeInput.addEventListener('keyup', () => {
              rangeInput.step = mouseStep;
            });
          }
        }
      });
    },
  };
})(jQuery, Drupal, once);
