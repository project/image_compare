# Image Compare Accessible Slider

This module provides a new formatter for image field
with Before After sliding effect to compare images.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/image_compare).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/image_compare).


## Requirements

This module requires no modules outside of Drupal core (Image).

This module requires the [image-compare](https://github.com/cloudfour/image-compare)
javascript library.

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### image-compare library installation

#### Manually

1. Download the Image Compare javascript library here:<br>
https://registry.npmjs.org/@cloudfour/image-compare/-/image-compare-1.0.5.tgz

   The original source code is available here:<br>
https://github.com/cloudfour/image-compare/releases/tag/1.0.5 .

2. If it does not already exist, create a folder named `libraries` at the web
   root of your Drupal site. Then, create a folder named `image-compare`
   inside the `libraries` folder (i.e. `/libraries/image-compare`).

3. Extract the compressed file you downloaded in step 1 inside the
   `/libraries/image-compare` folder (i.e., so that the `index.min.js`
   file is at `/libraries/image-compare/dist/index.min.js`).

#### Using composer (recommended)

If your `composer` configuration already properly handles
`composer.libraries.json` files, it  should be done automatically.
If not, please refer to the documentation below.

If you would like to install the `image-compare` library with composer, you
probably used the [drupal composer template](https://github.com/drupal-composer/drupal-project)
to setup your project. It's recommended to use [asset-packagist](https://asset-packagist.org/)
to install JavaScript libraries. So you will need to add the following to your
`composer.json` file into the repositories section:

```json
    {
        "type": "composer",
        "url": "https://asset-packagist.org"
    }
```

It's also needed to extend the 'installer-path' section:

```json
    "web/libraries/{$name}": [
        "type:drupal-library",
        "type:bower-asset",
        "type:npm-asset"
    ],
```
And add a new 'installer-types' section next to the 'installer-path' in the
'extra' section:

```json
    "installer-types": ["bower-asset", "npm-asset"],
```

Here is the `image-compare` library page on the Asset Packagist website:
https://asset-packagist.org/package/npm-asset/cloudfour--image-compare

## Configuration

1. Enable the module at Administration > Extend.
2. Select **Image Compare Accessible Slider** formatter in your multivalued
image field display configuration,
3. Remember that you have to upload at least 2 images.


## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
