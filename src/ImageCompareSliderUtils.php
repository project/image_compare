<?php

namespace Drupal\image_compare;

/**
 * Various utility methods.
 */
class ImageCompareSliderUtils {

  /**
   * Extract "key=value" pairs from multiline text.
   *
   * Function that parses a multiline string where each line contains a
   * key-value pair separated by an equal sign, and returns an associative
   * array of these keys and values.
   *
   * @param string $string
   *   The string to analyze.
   *
   * @return array
   *   The associative array containing the keys and values.
   */
  public static function parseKeyValueString($string): array {
    // Array to store the results.
    $result = [];
    // Splits the string into lines.
    $lines = explode("\n", $string);

    foreach ($lines as $line) {
      // Checks if the line is not empty.
      if (trim($line) != '') {
        // Splits the line in two at the equal sign.
        [$key, $value] = explode('=', $line, 2);
        // Cleans the white spaces around the key.
        $key = trim($key);
        // Cleans up white spaces around the value.
        $value = trim($value);
        if ($key != '') {
          // Adds the key-value pair to the result array.
          $result[$key] = $value;
        }
      }
    }

    // Return the associative array.
    return $result;
  }

}
