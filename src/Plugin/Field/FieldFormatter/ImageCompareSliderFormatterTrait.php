<?php

namespace Drupal\image_compare\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\image\ImageStyleInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\image_compare\ImageCompareSliderUtils as ICSUtils;

/**
 * Provides a trait for the Image Compare Slider field formatter.
 */
trait ImageCompareSliderFormatterTrait {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'starting_position' => '',
      'additional_options' => '',
      'caption_field' => '',
      'options_field' => '',
    ] + parent::defaultSettings();
  }

  /**
   * Adds an image style selector to the form elements.
   *
   * This method populates the provided form elements array with a dropdown
   * allowing users to select an image style. It integrates the available image
   * styles in the system and includes a link to configure image styles if the
   * user has the necessary permissions.
   *
   * @param array &$elements
   *   The form elements array to modify. This array will have a new
   *   'image_style' key with the configured selector.
   * @param array $settings
   *   The current formatter settings, used to prepopulate the default selection
   *   for the image style.
   */
  abstract protected function addImageStyleSelect(array &$elements, array $settings);

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $elements = [];

    $this->addImageStyleSelect($elements, $settings);

    $elements['starting_position'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Starting position'),
      '#description' => $this->t('The starting position is expressed as a percentage, ex: 70.  Defaults to 50 if empty.'),
      '#default_value' => $settings['starting_position'],
      '#placeholder' => 'Ex: 30',
    ];

    $elements['additional_options'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Additional options'),
      '#description' => $this->t('Allows to define additional options for the Image Compare Slider formatter.'),
      '#default_value' => $settings['additional_options'],
      '#placeholder' => 'Ex: label_text=Use the slider to control the visibility of the two images',
    ];

    $available_fields_for_options =
      $this->getEntityFieldsByType(['string_long']);
    $elements['options_field'] = [
      '#title' => $this->t('Options field'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('options_field'),
      '#empty_option' => $this->t('None'),
      '#options' => $available_fields_for_options,
      '#description' => $this->t('Specify the parent entity field that we should use to get entity specific slider configuration options.'),
    ];

    $available_fields_for_caption =
      $this->getRelatedEntityFieldsByType(['string', 'string_long']);
    $elements['caption_field'] = [
      '#title' => $this->t('Caption field'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('caption_field'),
      '#empty_option' => $this->t('None'),
      '#options' => $available_fields_for_caption,
      '#description' => $this->t('Specify the related entity field that we should use as a caption for slider images.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];

    if ($settings['image_style']) {
      $summary[] = $this->t('Image style: @image_style', ['@image_style' => $settings['image_style']]);
    }

    if ($settings['starting_position']) {
      $summary[] = $this->t('Starting position: @starting_position', ['@starting_position' => $settings['starting_position']]);
    }

    if (!empty($settings['options_field'])) {
      $summary[] = $this->t('Options field: @field', ['@field' => $settings['options_field']]);
    }

    if (!empty($settings['caption_field'])) {
      $summary[] = $this->t('Caption field: @field', ['@field' => $settings['caption_field']]);
    }

    return $summary;
  }

  /**
   * Build options array for rendering template.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   List of image entities to use form rendering slider.
   * @param array $cache_tags
   *   List of cache tags to update if needed, depending on options source.
   *
   * @return array
   *   Options for template.
   */
  protected function buildTemplateOptions(
    FieldItemListInterface $items,
    array &$cache_tags,
  ): array {

    $options = [];

    $settings = $this->getSettings();

    $options['starting_position'] = $settings['starting_position'] ?? '50';

    $additional_options = ICSUtils::parseKeyValueString($settings['additional_options']);

    if (!empty($settings['options_field'])) {
      $parent_entity = $items->getParent()->getEntity();
      if (isset($parent_entity)
        && $parent_entity->hasField($settings['options_field'])) {
        $field_items = $parent_entity->get($settings['options_field']);
        if (!$field_items->isEmpty()) {
          foreach ($field_items as $field_item) {
            $entity_specific_options = ICSUtils::parseKeyValueString($field_item->value);
            $cache_tags = Cache::mergeTags($cache_tags, $parent_entity->getCacheTags());
            $options = array_merge($options, $entity_specific_options);
          }
        }
      }
    }

    return array_merge($options, $additional_options);
  }

  /**
   * Generate image data for the Twig template from an image item.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item
   *   The image item.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $caption_entity
   *   The entity to get the image caption from.
   * @param \Drupal\image\ImageStyleInterface|null $style
   *   The image style to use for display.
   *
   * @return array|null
   *   The image data for the Twig template or NULL if file entity missing.
   */
  protected function buildImageData(
    $image_item,
    $caption_entity,
    ?ImageStyleInterface $style,
  ): array|null {
    $image_entity = $image_item->entity;
    if ($image_entity !== NULL) {
      $url = is_null($style)
        ? $this->fileUrlGenerator->generateAbsoluteString($image_entity->getFileUri())
        : $style->buildUrl($image_entity->getFileUri());

      $alt = $image_item->get('alt')->getValue();

      $image = [];
      $image['url'] = $url;
      $image['alt'] = $alt;

      $settings = $this->getSettings();
      if (!empty($settings['caption_field'])) {
        $caption_field = $settings['caption_field'];
        if (is_null($caption_entity)) {
          $this->setCaptionFromImageItem(
            $image_item, $caption_field, $image);
        }
        else {
          if ($caption_entity->hasField($caption_field)) {
            $this->setCaptionFromEntity(
              $caption_entity, $caption_field, $image);
          }
          // Handle image alt and title cases.
          else {
            $this->setCaptionFromImageItem(
              $image_item, $caption_field, $image);
          }
        }
      }

      return $image;
    }
    else {
      return NULL;
    }
  }

  /**
   * Load ImageStyle entity to use for display.
   *
   * @param array $settings
   *   Formatter settings.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   The ImageStyle object.
   */
  protected function getImageStyle(array $settings): ?ImageStyleInterface {
    return $this->loadImageStyle($settings['image_style']);
  }

  /**
   * Load ImageStyle entity to use for display.
   *
   * @param string $id
   *   Style identifier.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   The ImageStyle object.
   */
  protected function loadImageStyle(string $id): ?ImageStyleInterface {
    try {
      return $this->imageStyleStorage->load($id);
    }
    catch (\Exception $e) {
      Error::logException($this->loggerFactory->get('image_compare'), $e);
      return NULL;
    }
  }

  /**
   * Builds the CIS template render array.
   *
   * @param array $images
   *   Images array to add to the Image Compare slider.
   * @param array $options
   *   Configuration options for the Image Compare slider.
   * @param array $cache_tags
   *   Array for cache tags.
   *
   * @return array
   *   The Image Compare Accessible Slider render array.
   */
  protected function buildRenderArray(
    array $images,
    array $options,
    array $cache_tags,
  ): array {
    $id = Html::getUniqueId('image_compare_slider');
    return [
      '#theme' => 'image_compare_slider',
      '#id' => $id,
      '#images' => $images,
      '#options' => $options,
      '#cache' => [
        'tags' => $cache_tags,
      ],
      '#attached' => [
        'library' => [
          'image_compare/image_compare',
        ],
      ],
    ];
  }

  /**
   * Build a list of all text fields of the related entity.
   *
   * @return array
   *   The related entity text fields.
   */
  protected function getEntityFieldsByType(array $types): array {

    $found_fields = [];

    try {
      $entity_type = $this->fieldDefinition->getTargetEntityTypeId();
      $bundle = $this->fieldDefinition->getTargetBundle();

      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);

      foreach ($fields as $field_name => $field_definition) {
        // Check if the field type is in list of wanted types.
        if (in_array($field_definition->getType(), $types)) {
          $found_fields[$field_name] = $field_definition->getLabel();
        }
      }
    }
    catch (\Exception $e) {
      Error::logException($this->loggerFactory->get('image_compare'), $e);
    }

    return $found_fields;
  }

  /**
   * Build a list of all text fields of the related entity.
   *
   * @return array
   *   The related entity text fields.
   */
  protected function getRelatedEntityFieldsByType(array $types): array {

    $found_fields = [];

    $field_type = $this->fieldDefinition->getType();
    $field_settings = $this->fieldDefinition->getSettings();

    if ($field_type === 'image') {
      if ($field_settings['alt_field']) {
        $found_fields['alt'] = $this->t('Image Alt');
      }
      if ($field_settings['title_field']) {
        $found_fields['title'] = $this->t('Image Title');
      }
    }
    else {
      $found_fields['alt'] = $this->t('Image Alt (if available)');
      $found_fields['title'] = $this->t('Image Title (if available)');
    }

    if ($field_type === 'entity_reference') {
      try {
        $handler_settings =
          $this->fieldDefinition->getSetting('handler_settings');

        $entity_type = $field_settings['target_type'];
        $bundles = $handler_settings['target_bundles'];

        foreach ($bundles as $bundle) {
          $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
          foreach ($fields as $field_name => $field_definition) {
            // Check if the field type is in list of wanted types.
            if (in_array($field_definition->getType(), $types)) {
              $found_fields[$field_name] =
                $field_definition->getLabel() . ' (' . $field_name . ')';
            }
          }
        }
      }
      catch (\Exception $e) {
        Error::logException($this->loggerFactory->get('image_compare'), $e);
      }
    }

    return $found_fields;
  }

  /**
   * Defines slider image caption from referenced entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to get the caption from.
   * @param string $caption_field
   *   The caption field name.
   * @param array $image
   *   The image array to set the caption to.
   */
  protected function setCaptionFromEntity(
    ContentEntityInterface $entity,
    string $caption_field,
    array &$image,
  ): void {
    try {
      $caption_field = $entity->get($caption_field);
      if (isset($caption_field[0])) {
        $caption = $caption_field[0]->value;
        if (!empty($caption)) {
          $image['caption'] = $caption;
        }
      }
    }
    catch (\Exception $e) {
      Error::logException($this->loggerFactory->get('image_compare'), $e);
    }
  }

  /**
   * Defines slider image caption from image item.
   *
   * @param \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item
   *   The entity to get the caption from.
   * @param string $caption_field
   *   The caption field name.
   * @param array $image
   *   The image array to set the caption to.
   */
  protected function setCaptionFromImageItem(
    ImageItem $image_item,
    string $caption_field,
    array &$image,
  ): void {
    try {
      $caption = $image_item->get($caption_field)->getValue();
      if (!empty($caption)) {
        $image['caption'] = $caption;
      }
    }
    catch (\Exception $e) {
      Error::logException($this->loggerFactory->get('image_compare'), $e);
    }
  }

}
